> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Advanced Mobiel app development 

## Joshua Barrios 

### Assignment #1 Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. Development installations 
3. Chapter questions (ch1, 2)
4. Bitbucket repo links 

#### README.md file should include the following items:

* screenshot of running java hello
* screenshot of running android studio - My first app
* screenshot of running android studio - Contacts app
* git commands with short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - this initializes the repo 
2. git status - tells you what state your files are in 
3. git add - adds all or specified files to be ready to commit 
4. git commit - commits changes to be ready for push
5. git push - pushes all the files to the repo 
6. git pull - pulls all the code from an existing repo
7. git rebase - moving or combining commits to a new base commit

#### Assignment Screenshots:


*Screenshot of running java Hello*:

![JDK Installation Screenshot](helloJava.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](HelloApp.png)

*Screenshot of Contacts App - Main Screen*:
 ![Android Studio Screenshot](home.png)
 *Screenshot of Contacts App - Info Screen*:
 ![Android Studio Screenshot](info.png)
#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
