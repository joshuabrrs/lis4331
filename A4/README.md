> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Adv. Android Development

## Joshua Barrios

### Assignment #4 Requirements:


1. Include splash screen image, app title, intro text.
2. Include appropriate images.
3. Must use persistent data: SharedPreferences
4. Widgets and images must be vertically and horizontally aligned.
5. Must add background color(s) or theme
6. Create and displaylauncher icon image
7. Create an app that calculated home mortgage interest



#### README.md file should include the following items:


* Screenshot of unpopulated app
* Screenshot of populated app 
* Screenshot of error
* screenshot of splash




#### Assignment Screenshots:
|*Screenshot of unpopulated app*:               | *Screenshot of populated app*:                |
|---------------------------------------------- |---------------------------------------------- |
|![unpopulated app Screenshot](empty.png)       |![populated app Screenshot](populated.png)     |
|                                               |                                               |
|                                               |

|*Screenshot of error*:                         | *Screenshot of splash*:                       |
|---------------------------------------------- |---------------------------------------------- |
|![error Screenshot](error.png)                 |![splash Screenshot](splash.png)               |
|                                               |                                               |
|                                               |                                               |
