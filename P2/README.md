
# Adv. Android Development

## Joshua Barrios

### Project #2 Requirements:


1. Create an app that show list of things to do
2. Uses databases



#### README.md file should include the following items:


* Screenshot of splash app
* Screenshot of app list





#### Assignment Screenshots:
|*Screenshot of  app in launcher*:              | *Screenshot of app list*:                   |
|---------------------------------------------- |---------------------------------------------- |
|![splash app Screenshot](launcher.png)         |![list app Screenshot](list.png)            |
|                                               |                                               |
|                                               |


