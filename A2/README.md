> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Adv. Android Development

## Joshua Barrios

### Assignment #2 Requirements:


1. Create an app that provides tip on number of guests



#### README.md file should include the following items:


* Screenshot of unpopulated app
* Screenshot of populated app 




#### Assignment Screenshots:
|*Screenshot of unpopulated app*:               | *Screenshot of populated app*:                |
|---------------------------------------------- |---------------------------------------------- |
|![unpopulated app Screenshot](unpopulated.png) |![populated app Screenshot](populated.png)     |
|                                               |
|                                               |


