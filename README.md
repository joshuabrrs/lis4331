> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 4331 Android development

## Joshua Barrios

### LIS4930 Requirements:

*Course Work Links:*

1. [A1 README.md](A1/README.md "My A1 README.md file")
    - Install JDK 
    - Install Android Studio and create My First App and Contacts app
    - provide screenshots of installations 
    - create bitbucket repo 
    - complete bitbucket tutorials 
    - provide git command descriptions

2. [A2 README.md](A2/README.md "My A2 README.md file")
    - Create an application that is a tip calculator 
    - Provide screenshots of working app 
3. [A3 README.md](A3/README.md "My A3 README.md file")
    - Create an application that is currency converter
    - Provide screenshots of working app 

4. [P1 README.md](P1/README.md "My P1 README.md file")
    - Create an application that plays music
    - Provide screenshots of working app 

5. [A4 README.md](A4/README.md "My A4 README.md file")
    - Include splash screen image, app title, intro text.
    - Include appropriate images.
    - Must use persistent data: SharedPreferences
    - Widgets and images must be vertically and horizontally aligned.
    - Must add background color(s) or theme
    - Create and displaylauncher icon image
    - Create an app that calculated home mortgage interest
6. [A5 README.md](A5/README.md "My A5 README.md file")
    - Include splash screen showing articles.
    - Include appropriate images.
    - Must use persistent RSS feed
    - Must add background color(s) or theme
    - Create and displaylauncher icon image
    - Create an app that shows news articles
7. [P2 README.md](P2/README.md "My P2 README.md file")
    - Create an app that show list of things to do
    - Uses databases
    

