
# Adv. Android Development

## Joshua Barrios

### Project #1 Requirements:


1. Create an app that plays music



#### README.md file should include the following items:


* Screenshot of app playing
* Screenshot of app paused
* Screenshot of app in splash




#### Assignment Screenshots:
|*Screenshot of  app in splash*:                | *Screenshot of app paused*:                   |
|---------------------------------------------- |---------------------------------------------- |
|![splash app Screenshot](splash.png)           |![paused app Screenshot](pause.png)            |
|                                               |                                               |
|                                               |

|*Screenshot of app playing*:                   | 
|---------------------------------------------- |
|![play app Screenshot](play.png)               |
|                                               |      
|                                               |
