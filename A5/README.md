> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Adv. Android Development

## Joshua Barrios

### Assignment #5 Requirements:


1. Include splash screen showing articles.
2. Include appropriate images.
3. Must use persistent RSS feed
5. Must add background color(s) or theme
6. Create and displaylauncher icon image
7. Create an app that shows news articles



#### README.md file should include the following items:


* Screenshot of splash screen with articles
* Screenshot of article title
* Screenshot of article





#### Assignment Screenshots:
|*Screenshot of splash*:             | *Screenshot of article title*:      |*Screenshot of article*:                    |
|------------------------------------|-------------------------------------|--------------------------------------------|
|![splash app Screenshot](splash.png)|![artice app Screenshot](clicked.png)|![article Screenshot](article.png)          |
|                                    |                                     |                                            |
|                                    |                                     |                                            |

                                          
